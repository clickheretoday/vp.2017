package ru.mirea.ippo.vp;

/**
 * Created by a4 on 25.03.2017.
 */
public class FirstChild extends FirstRealObject {

    public void firstChildSpecific() {
        System.out.println("FirstChild.firstChileSpecific");
    }

    protected void protectedMethod(int i) {
        super.protectedMethod();
    }

    @Override
    protected void protectedMethod() {
        super.protectedMethod();
    }

    @Override
    public int getFoo() {
        return super.getFoo();
    }
}
