package ru.mirea.ippo.vp.pack;

import ru.mirea.ippo.vp.FirstChild;
import ru.mirea.ippo.vp.FirstRealObject;
import ru.mirea.ippo.vp.pack.SecondChild;

import java.util.ArrayList;

/**
 * Created by a4 on 11.02.2017.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello, world!");

        final int MY_FINAL_INT = 4242;

        boolean bool;
        bool = true;
        bool = false;

        double db = 1.2d;
        float fl = 0.1f;

        int i = 422;
        long l = 1;
        short s = 1;
        byte b = (byte) i;

        System.out.println(-256 + (422 % 256));
        System.out.println("Byte overflow: " + b);

        int intInBinaryForm = 0b1111;
        System.out.println("Binary int: " + intInBinaryForm);
        System.out.println("Binary int in binary form: " + Integer.toString(intInBinaryForm, 2));
        int intInOctForm = 010;
        System.out.println("Oct int: " + intInOctForm);
        // try to think about why it is -1
        long intInHexForm = 0xFFFFFFFF;
        System.out.println("Hex int: " + intInHexForm);

        Integer myNewInteger = 42;
        System.out.println("myNewInteger / db = " + (myNewInteger / db));
        System.out.println("200/ 99 = " + (200 / 99));
        System.out.println("200/ 99.0 = " + (200 / 99.0));
        System.out.println("200/ (double) 99 = " + (200 / (double) 99));
        System.out.println("2.1 + 1.01 = " + (2.1 + 1.01));

        Long myNewLong = 42L;
        Short myNewShort = 42;
        Double myNewDouble = 42d;
        Float myNewFloat = 42f;
        Boolean myNewBoolean = true;

        int[] intArray;
        intArray = new int[42];
        intArray = new int[]{12, 332, 35, 664};
        intArray[0] = 42;
//        intArray[1] = 0.1; // error
        System.out.println("intArray[0] = " + intArray[0]);
        System.out.println("intArray[1] = " + intArray[1]);
        System.out.println("intArray[3] = " + intArray[3]);
        System.out.println("intArray.length = " + intArray.length);

        intArray = new int[43];
        System.out.println("After reinitialization intArray[0] = " + intArray[0]);

        Integer[] integerArray = new Integer[4242];
        integerArray[0] = 43;
        integerArray[1] = new Integer(42);
        System.out.println("integerArray[0] = " + integerArray[0]);
        System.out.println("integerArray[1] = " + integerArray[1]);

        FirstRealObject myObject = new FirstRealObject();

        System.out.println("---");

        FirstRealObject mainObjectFoo = new FirstRealObject(24);

        System.out.println("----");

        System.out.println("FirstRealObject Object final int: " + myObject.MY_FINAL_INT);
        System.out.println("FirstRealObject Object FOO final int: " + mainObjectFoo.MY_FINAL_INT);

        System.out.println("FirstRealObject Object final bool: " + myObject.MY_FINAL_BOOL);
        System.out.println("FirstRealObject Object FOO final bool: " + mainObjectFoo.MY_FINAL_BOOL);

        myObject.methodFoo();
        mainObjectFoo.methodFoo();

        System.out.println("---");

        System.out.println(myObject.getFoo());
        System.out.println(mainObjectFoo.getFoo());

        System.out.println("---");

        myObject.setFoo(22);
        System.out.println(myObject.getFoo());
        System.out.println(mainObjectFoo.getFoo());

        System.out.println("---");


        System.out.println(myObject.getBar());
        System.out.println(myObject.getAge());

//        int boo;
//        System.out.println("boo: " + boo);
//        System.out.println(myObject.fooBarPrivate);

        myObject.foo = -43;
        myObject.setAge(-42);
        System.out.println("myObject.getAge() " + myObject.getAge());


        myObject.setAge(-42, true);
        System.out.println("myObject.getAge() " + myObject.getAge());

        mainObjectFoo.showAllValuesInArray(false, 1, 2, 3, 4);

        ArrayList<Object> arrayList = new ArrayList<>();

        int var = 4455;

        myObject.isReference(var);

        if (var == 4455) {
            System.out.println("By value");
        } else if (var == 42) {
            System.out.println("By reference");
        }

//        myObject.isReference(mainObjectFoo);
        System.out.println(mainObjectFoo.getAge());

        myObject.performAction(new Double(42.0));

        System.out.println("=============");

//        myObject.performAction(null);

        // wont work cause of boxing
//        Double d1 = null;
//
//        d1.toString();
//
        Double d = Double.NaN;
        double dd = Double.NaN;

//        while(dd != dd)
//            System.out.println("!");

//        new FirstRealObject(1, 2, 3, 4.2, new Object());
//        myObject.FirstRealObject();


        Object obj = null;
        obj = 42;
        obj = 42.0;
        obj = true;
        obj = new FirstRealObject();

        System.out.println(new FirstRealObject());

//        myObject.privateMethod();
//        myObject.protectedMethod();
//        myObject.defaultMethod();

        FirstChild firstChild = new FirstChild();
        SecondChild secondChild = new SecondChild();

//        firstChild.defaultMethod();
//        firstChild.protectedMethod();
//        firstChild.protectedMethodCallPrivate();
        firstChild.getFoo();
        int bar1 = firstChild.bar;

//        secondChild.protectedMethod();
//        secondChild.protectedMethodCallPrivate();
        secondChild.getFoo();
        int bar = secondChild.bar;

        FirstRealObject fro = new FirstChild();
        fro.isReference(fro);

    }
}
