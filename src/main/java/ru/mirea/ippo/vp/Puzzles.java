package ru.mirea.ippo.vp;

/**
 * Created by a4 on 25.03.2017.
 */
public class Puzzles {

    public static void main(String[] args) {
        puzzle5();
    }

    public static void puzzle1() {
        short x = 2;
        int i = 1;

        x += i; // correct

//        x = x + i; // incorrect
    }


    /**
     * x+=i will work after 1.4 Java release
     */
    public static void puzzle2() {
        Object x = "1";
        String i = "2";

        x = x + i; // correct

        x += i; // incorrect
    }

    public static void puzzle3() {
        System.out.println(0x90);
        for (byte b = Byte.MIN_VALUE; b < Byte.MAX_VALUE; b++) {
            if (b == 0x90) {
                System.out.println("=)");
            }
        }
    }

    public static void puzzle4() {
        double i = Double.POSITIVE_INFINITY;
        while (i == (i + 1)) {
            System.out.println("Wheeeeee");
        }
    }

    public static void puzzle5() {
        // i,j  - ??
        Integer i = 42;
        Integer j = 42;

        if (i <= j && j <= i && i != j) {
            System.out.println("Wheeeeee");
        } else {
            System.out.println("No =(");
        }
    }
}
