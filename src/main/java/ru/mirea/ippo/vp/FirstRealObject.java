package ru.mirea.ippo.vp;

/**
 * Created by a4 on 18.03.2017.
 */
public class FirstRealObject extends Object {
    /**
     * DON'T NAME LIKE THIS THIS. Should be main() Because is obscures the Main constructor
     */
//    public void Main() {
//        System.out.println("Main.Main method");
//    }

    private Double d; // = null;

    private int age;

    public final boolean MY_FINAL_BOOL = true;
    public final int MY_FINAL_INT;
    public int foo = 42;
    /**
     * 0
     */
    public int bar;

    public int fooBar;
    private int fooBarPrivate;
    protected int fooBarProtected;
    int fooBarDefault;

    public FirstRealObject() {
        this(42);
        System.out.println("Constructor FirstRealObject.FirstRealObject");
    }

    public FirstRealObject(int i) {
        MY_FINAL_INT = i;
        System.out.println("FirstRealObject.FirstRealObject");
        System.out.println("i = [" + i + "]");

    }

    public FirstRealObject(int i, int a, int b, double x, Object o) {
        MY_FINAL_INT = i;
        System.out.println("FirstRealObject.FirstRealObject");
        System.out.println("i = [" + i + "]");
    }

    public void methodFoo() {
        System.out.println("FirstRealObject.methodFoo");
    }

    public boolean isMY_FINAL_BOOL() {
        return MY_FINAL_BOOL;
    }

    public int getMY_FINAL_INT() {
        return MY_FINAL_INT;
    }

    public int getFoo() {
        return foo;
    }

    public void setFoo(int foo) {
        this.foo = foo;
    }

    public int getBar() {
        return bar;
    }

    public void setBar(int bar) {
        this.bar = bar;
    }

    public int getFooBarPrivate() {
        return fooBarPrivate;
    }

    public void setFooBarPrivate(int fooBarPrivate) {
        this.fooBarPrivate = fooBarPrivate;
    }

    public int getAge() {
        return age;
    }

    /**
     * Sets an age
     *
     * @param age age must not be negative
     */
    public void setAge(int age) {
        setAge(age, false);
    }

    public void setAge(int age, boolean hack) {
        if (hack || (age >= 0 && age != 0)) {
            this.age = age;
        } else {
            System.out.println("Age can not be negative: " + age);
            System.out.println("Please provide correct data!");
        }
    }

//    public void showAllValuesInArray(int[] i) {
//
//    }

    public void showAllValuesInArray(boolean b, int... i) {
        int[] j = i;
        System.out.println("i.length: [" + i.length + "]");
        System.out.println("j.length: [" + j.length + "]");

        for (int k = 0;
             k < i.length;
             k++) {
            System.out.println("i[k]: i[" + k + "]:" + i[k]);
        }
    }

    public void isReference(int i) {
        i = 42;
    }

    public void isReference(FirstRealObject i) {
//        i = new FirstRealObject();
//        i.setAge(55);

        ((FirstChild) i).firstChildSpecific();
    }

    public void performAction(Double d) {
        String s = "performAction: ";
        System.out.println(s + d.toString());
    }

    protected void protectedMethod() {
        System.out.println("FirstRealObject.protectedMethod");
    }

    void defaultMethod() {
        System.out.println("FirstRealObject.defaultMethod");
    }

    protected void protectedMethodCallPrivate() {
        System.out.println("FirstRealObject.protectedMethodCallPrivate");
        privateMethod();
    }

    private void privateMethod() {
        System.out.println("FirstRealObject.privateMethod");
    }
}
