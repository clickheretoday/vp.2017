package ru.mirea.ippo.vp.humanreadable.earth;

/**
 * Created by a4 on 01.04.2017.
 */
public enum Planets {
    Venus(22), Mercury(33), Earth(42);

    private final int mass;

    Planets(int mass) {
        this.mass = mass;
    }

    public int getMass() {
        return mass;
    }
}
