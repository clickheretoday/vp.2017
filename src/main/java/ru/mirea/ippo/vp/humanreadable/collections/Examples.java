package ru.mirea.ippo.vp.humanreadable.collections;

import java.util.*;

/**
 * Created by a4 on 06.05.2017.
 */
public class Examples {

    public static void main(String[] args) {
        /*
         more information about complexity at http://bigocheatsheet.com/
         */

        // read: O(1)
        // write: best: O(1), worst: O(n)

        final ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Foo");
        arrayList.add("bar");

        System.out.println(arrayList.get(1));
        arrayList.remove(0);
        System.out.println(arrayList.get(0));

        System.out.println("-------------");

        // Write: best: O(1), worst: O(n)
        // Read: best: O(1), worst: O(n)
        final LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("Foo");
        linkedList.add("bar");

        System.out.println(linkedList.get(1));
        linkedList.remove(1);
        System.out.println(linkedList.get(0));

        System.out.println("-------------");
        final HashSet<String> hashSet = new HashSet<>();
        hashSet.add("Foo");
        hashSet.add("Foo");
        hashSet.add("bar");

        System.out.println("hasSet.size: " + hashSet.size());

        final HashMap<String, String> hashMap = new HashMap<>(42);
        hashMap.put("keyFoo", "Foo");
        hashMap.put("keyFoo", "FooBar");
        hashMap.put("keybar", "bar");


        final LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>(42);
    }
}
