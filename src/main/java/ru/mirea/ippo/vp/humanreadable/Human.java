package ru.mirea.ippo.vp.humanreadable;

/**
 * Created by a4 on 25.03.2017.
 */
public abstract class Human implements Creature {

    private final String name;
    private int age;
    /**
     * True is man, false is woman
     */
    protected final boolean sex;

    /**
     * @param name
     * @param age
     * @param sex  true is man, false is woman
     */
    protected Human(String name, int age, boolean sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    /**
     * Sets an age
     *
     * @param age age must not be negative
     */
    public void setAge(int age) {
        setAge(age, false);
    }

    public void setAge(int age, boolean hack) {
        if (hack || (age >= 0 && age != 0)) {
            this.age = age;
        } else {
            System.out.println("Age can not be negative: " + age);
            System.out.println("Please provide correct data!");
        }
    }

    /**
     * @return true is man, false is woman
     */
    public boolean getSex() {
        return sex;
    }

    public abstract boolean isAlive();
}
