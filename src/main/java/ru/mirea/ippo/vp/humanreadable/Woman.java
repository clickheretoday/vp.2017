package ru.mirea.ippo.vp.humanreadable;

/**
 * Created by a4 on 25.03.2017.
 */
public class Woman extends Human {

    private boolean pregnant = false;

    public Woman(String name, int age) {
        super(name, age, false);
    }

    public boolean isPregnant() {
        return pregnant;
    }

    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    @Override
    public void consume(Creature c) {

    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public void setAlive(boolean alive) {

    }
}
