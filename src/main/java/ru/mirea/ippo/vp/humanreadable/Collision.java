package ru.mirea.ippo.vp.humanreadable;

/**
 * Created by a4 on 01.04.2017.
 */
public interface Collision {

    default boolean isAlive() {
        System.out.println("Collision.isAlive");
        return false;
    }

    void setAlive(boolean alive);

    default void walk(){
        System.out.println("Collision.walk");
    }
}
