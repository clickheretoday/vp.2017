package ru.mirea.ippo.vp.humanreadable.earth;

import ru.mirea.ippo.vp.humanreadable.*;

/**
 * Created by a4 on 25.03.2017.
 */
public class Earth {

    public static void main(String[] args) {
//        Human x = new Human("x", -1);

        Man adam = new Man("Adam", 42);
        Woman eve = new Woman("Eve", 24);

        printCommonInfo(adam);
        printCommonInfo(eve);

        printInfo(eve);

        eve.setPregnant(true);
        printInfo(eve);

        System.out.println("---");

        printSpecificInfo(eve);
        printSpecificInfo(adam);

        System.out.println("---");

        printSpecificInfoBroken(eve);

        System.out.println("---");

        printIsAlive(adam);

        System.out.println("Collision: " + ((Collision) adam).isAlive());
        System.out.println("----");
        System.out.println("Human: " + ((Human) adam).isAlive());
        System.out.println("-----");
        System.out.println("Man: " + (adam).isAlive());

        ((Alivable) adam).isAlive();
        ((Collision) adam).isAlive();
        adam.isAlive();

        System.out.println(Planets.Earth.getMass());

//        printSpecificInfoBroken(adam);

        // implement visitor pattern
    }

    private static void printSpecificInfoBroken(Human human) {
        System.out.println(((Woman) human).isPregnant());
    }

    private static void printSpecificInfo(Creature creature) {
        if (creature instanceof Human) {
            Human human = (Human) creature;

            if (human instanceof Woman) {
                Woman woman = (Woman) human;
                printInfo(woman);
            } else if (human instanceof Man) {
                Man man = (Man) human;
                printInfo(man);
            }
        }
    }

    private static void printInfo(Woman woman) {
        System.out.println("Is " + woman.getName() + " pregnant? "
                + (woman.isPregnant() ? "Yes" : "No"));
    }


    private static void printInfo(Man man) {
        System.out.println("=(");
    }

    private static void printCommonInfo(Human human) {
        System.out.println(human.getName() + " is " + human.getAge() + " year old. " +
                (human.getSex() ? "Man" : "Woman"));
    }

    private static void printIsAlive(Alivable a) {
        System.out.println("Is alive?: [" + a.isAlive() + "]");
    }
}
