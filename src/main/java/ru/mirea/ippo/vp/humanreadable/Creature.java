package ru.mirea.ippo.vp.humanreadable;

/**
 * Created by a4 on 25.03.2017.
 */

/**
 * interface
 * abstract class
 * enum
 */

public interface Creature extends Alivable, Collision {

    void consume(Creature c);

    default void die() {
        System.out.println("Creature.die");
    }

    @Override
    default boolean isAlive() {
        return false;
    }
}
