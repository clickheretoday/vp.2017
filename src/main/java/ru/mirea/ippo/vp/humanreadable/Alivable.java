package ru.mirea.ippo.vp.humanreadable;

/**
 * Created by a4 on 01.04.2017.
 */
public interface Alivable {

    default boolean isAlive() {
        System.out.println("Alivable.isAlive");
        return false;
    }

    void setAlive(boolean alive);
}
