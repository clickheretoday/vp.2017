package ru.mirea.ippo.vp.humanreadable;

/**
 * Created by a4 on 25.03.2017.
 */
public class Man extends Human {
    public Man(String name, int age) {
        super(name, age, true);
    }

    @Override
    public boolean isAlive() {
        System.out.println("Man.isAlive");

        return false;
    }

    @Override
    public void setAlive(boolean alive) {

    }

    @Override
    public void consume(Creature c) {

    }

    @Override
    public void walk() {

    }
}
