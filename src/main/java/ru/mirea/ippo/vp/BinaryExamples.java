package ru.mirea.ippo.vp;

public class BinaryExamples {

    public static void main(String args[]) {
        int iMin = Integer.MIN_VALUE;
        printInOctAndBit("iMin", iMin);
        printInOctAndBit("iMin + 1", iMin + 1);

        int h = -1;
        printInOctAndBit("h >>> 1", h >>> 1);
        printInOctAndBit("~h", ~h);
        printInOctAndBit("(~h) >>> 1", (~h) >>> 1);

        System.out.println("---");

        h = 3;
        System.out.println(~h);
        System.out.println(Integer.toString((~h) >>> 1));
        System.out.println(Integer.toString((~h) >>> 1, 2));

        int x = Integer.MIN_VALUE;
        int y = Integer.MAX_VALUE;
        printInOctAndBit("X", x);
        printInOctAndBit("Y", y);
        x = y - x;
        System.out.println("X tmp: " + x);
        y = y - x;
        System.out.println("Y swapped: " + y);
        x = y + x;
        System.out.println("X swapped: " + x);

        char one = 88;
        char two = 89;

        System.out.println("\nOne: " + one);
        one = (char) (one ^ two);
        System.out.println("One ^: " + one);
        two = (char) (one ^ two);
        System.out.println("Two swapped: " + two);
        one = (char) (one ^ two);
        System.out.println("One swapped: " + one);

        two = (char) ((one ^= (two ^= one)) ^ two);
        System.out.println("Two swapped again: " + two);
        System.out.println("One swapped again: " + one);

        System.out.println("\nBinary shifting >>");

        int i = 0b1111_1111_1111_1111_1111_1111_1111_0000;
        System.out.println(Integer.toString(i, 2));
        System.out.println(Integer.toString(i, 10));
        System.out.println("\nShifting i >>");
        int shift_i = i >> 2;
        System.out.println(Integer.toString(shift_i, 2));
        System.out.println(Integer.toString(shift_i, 10));
        System.out.println("\n----\n");

        int a = 0b0011_1100;	/*
         * 60 = 0011 1100
         */

        int b = 0b0000_1101;	/*
         * 13 = 0000 1101
         */
        printInOctAndBit("a", a);
        printInOctAndBit("b", b);
        int c = 0;

        c = a & b;       /*
         * 12 = 0000 1100
         */

        printInOctAndBit("a & b ", c);

        c = a | b;       /*
         * 61 = 0011 1101
         */

        printInOctAndBit("a | b", c);

        c = a ^ b;       /*
         * 49 = 0011 0001
         */

        printInOctAndBit("a ^ b ", c);

        c = ~a;          /*
         * -61 = 1100 0011
         */

        printInOctAndBit("~a ", c);

        c = a << 32; // a << shift, where shift = shift mod 32

        printInOctAndBit("a << 32 ", c);

        c = a << 2;
        /*
         * 240 = 1111 0000
         */

        printInOctAndBit("a << 2 ", c);

        c = a >> 2;     /*
         * 15 = 1111
         */

        printInOctAndBit("a >> 2", c);

        c = a >>> 2;
        /*
         * 15 = 1111
         */

        printInOctAndBit("a >>> 2", c);

        c = 2 >>> a;

        printInOctAndBit("2 >>> a ", c);
    }

    public static void printInOctAndBit(String varName, int i) {
        System.out.println(varName + " = " + Integer.toString(i));
        System.out.println(varName + "(bin) = " + Integer.toString(i, 2));
        System.out.println();
    }
}
