package ru.mirea.ippo.vp;

import java.util.Arrays;

/**
 * Created by a4 on 11.02.2017.
 */
public enum EMails {
    ISMO_1_16("ismo0116@mail.ru"),
    ISMO_5_16("ev.eliseeff@ya.ru", "daemonshi@mail.ru", "clickheretoday@gmail.com"),
    ISMO_8_16("ismo-08-16@mail.ru");

    private final String[] emails;

    EMails(String... emails) {
        this.emails = emails;
    }

    public String[] getEmails() {
        return emails;
    }

    public static String getAllEmails() {
        final StringBuilder sb = new StringBuilder();
        Arrays.stream(values()).forEach(eMails ->
                Arrays.stream(eMails.getEmails()).forEach(s -> {
                    if (sb.length() != 0)
                        sb.append(", ");
                    sb.append(s);
                }));

        return sb.toString();
    }
}