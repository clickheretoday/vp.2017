package ru.mirea.ippo.vp.chat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ServerStarter {

    public static final int PORT = 4242;

    public static void main(String[] args) throws IOException {
        final Server server = new Server(PORT);
        server.start();

        new Thread(() -> {
            try (BufferedReader consoleIn = new BufferedReader(new InputStreamReader(System.in))) {
                while (!consoleIn.ready()) {
                    if ("quit".equals(consoleIn.readLine())) {
                        server.stop();
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}
