package ru.mirea.ippo.vp.chat.protocol;

public class VLineProtocol implements IProtocol {
    public final String DELIMITER = "|";
    public final String DELIMITER_ESCAPED = "\\" + DELIMITER;
    public final int SECTIONS_COUNT = 2;

    public String encodeMessage(Message msg) {
        return msg.getType().getCode() + DELIMITER + msg.getContent();
    }

    public Message parseMessage(String msg) throws ParsingException {
        String[] split = msg.split(DELIMITER_ESCAPED);
        //LOGGER.log(Level.FINE, "Client message parsed: {0}", new Object[]{Arrays.toString(split)});

        Message.Type type;
        if (split.length != SECTIONS_COUNT || (type = Message.Type.findType(split[0])) == null)
            throw new ParsingException();

        return new Message(type, split[1]);
    }
}