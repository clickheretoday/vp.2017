package ru.mirea.ippo.vp.chat.protocol;

public class Message {
    public enum Type {
        LOGIN("l"), CHAT("c"), SERVICE("s");
        private final String code;

        Type(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public static Type findType(String code) {
            for (Type type : Type.values()) {
                if (type.getCode().equals(code))
                    return type;
            }
            return null;
        }
    }

    private final Type type;
    private final String content;

    public Message(Type type, String content) {
        this.type = type;
        this.content = content;
    }

    public Type getType() {
        return type;
    }

    public String getContent() {
        return content;
    }
}
