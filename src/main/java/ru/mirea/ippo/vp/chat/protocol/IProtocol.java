package ru.mirea.ippo.vp.chat.protocol;

public interface IProtocol {
    String encodeMessage(Message msg);

    Message parseMessage(String msg) throws ParsingException;
}
