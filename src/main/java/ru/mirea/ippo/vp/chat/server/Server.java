package ru.mirea.ippo.vp.chat.server;

import ru.mirea.ippo.vp.chat.protocol.IProtocol;
import ru.mirea.ippo.vp.chat.protocol.Message;
import ru.mirea.ippo.vp.chat.protocol.ParsingException;
import ru.mirea.ippo.vp.chat.protocol.VLineProtocol;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
    private final IProtocol PROTOCOL = new VLineProtocol();
    private final Set<ClientHandler> clientHandlers = ConcurrentHashMap.newKeySet();


    private final int port;
    private ServerSocket serverSocket;

    public Server(int port) {
        this.port = port;
    }

    public synchronized void stop() throws IOException {
        LOGGER.info("Stopping...");
        clientHandlers.forEach(clientHandler -> {
            try {
                clientHandler.getClientSession().close();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Can't close client's session", e);
            }
        });
        serverSocket.close();
    }

    public synchronized void start() {
        LOGGER.log(Level.INFO, "Server started on port {0}", new Object[]{port});
        Thread mainThread = new Thread(() -> {
            try (ServerSocket srv = new ServerSocket(port)) {
                this.serverSocket = srv;
                while (true) {
                    LOGGER.info("Waiting for new connection");

                    final Socket clientSocket = serverSocket.accept();

                    ClientSession clientSession = new ClientSession(clientSocket);

                    LOGGER.log(Level.INFO, "Client connected {0}, number: {1}", new Object[]{clientSocket.getRemoteSocketAddress().toString(), clientHandlers.size()});

                    final ClientHandler clientHandler = new ClientHandler(clientSession);
                    clientHandlers.add(clientHandler);

                    final Thread thread = new Thread(clientHandler);
                    thread.setName("Client_" + clientHandlers.size());
                    thread.start();
                }
            } catch (SocketException e) {
                LOGGER.warning("Server is closing...");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        mainThread.setName("ChatSrv_" + port);
        mainThread.start();
    }

    private void broadcast(String message) {
        clientHandlers.forEach(clientHandler -> {
            ClientSession clientSession = clientHandler.getClientSession();
            LOGGER.log(Level.FINE, "Sending to client''s session: {0}@{1}", new Object[]{clientSession, clientSession.getSocket().getRemoteSocketAddress().toString()});
            if (clientSession.getState() == ClientSession.State.LOGGED_IN) {
                try {
                    clientSession.write(PROTOCOL.encodeMessage(new Message(Message.Type.CHAT, message)));
                } catch (Exception e) {
                    LOGGER.log(Level.WARNING, "Unable to send message to client {0}", new Object[]{clientSession});
                }
            }
        });
    }

    private class ClientHandler implements Runnable {
        private final ClientSession clientSession;

        public ClientHandler(ClientSession clientSession) {
            this.clientSession = clientSession;
        }

        public ClientSession getClientSession() {
            return clientSession;
        }

        @Override
        public void run() {
            try {
                String clientInput = null;

                // dirty hack here
                try (ClientSession _clientSession = clientSession) {
                    while ((clientInput = clientSession.read()) != null) {
                        LOGGER.log(Level.INFO, "Client sent: {0}", new Object[]{clientInput});

                        Message clientMessage;
                        try {
                            clientMessage = PROTOCOL.parseMessage(clientInput);

                            switch (clientSession.getState()) {
                                case CONNECTED:
                                    if (clientMessage.getType() == Message.Type.LOGIN) {
                                        if (authenticateClient(clientMessage.getContent())) {
                                            clientSession.setState(ClientSession.State.LOGGED_IN);
                                            clientSession.setLogin(clientMessage.getContent());
                                            clientSession.write(PROTOCOL.encodeMessage(new Message(Message.Type.SERVICE, "Hello, " + clientMessage.getContent() + ", you may chat now")));
                                        } else {
                                            clientSession.setState(ClientSession.State.CONNECTED);
                                            clientSession.write(PROTOCOL.encodeMessage(new Message(Message.Type.SERVICE, "Bad login, try again")));
                                        }
                                    } else {
                                        clientSession.write(PROTOCOL.encodeMessage(new Message(Message.Type.SERVICE, "Unexpected message type! Expecting LOGIN here")));
                                    }
                                    break;
                                case LOGGED_IN:
                                    switch (clientMessage.getType()) {
                                        case CHAT:
                                            String message = clientSession.getLogin() + " says: " + clientMessage.getContent();
                                            broadcast(message);
                                            break;
                                        default:
                                            clientSession.write(PROTOCOL.encodeMessage(new Message(Message.Type.SERVICE, "Unsupported message type! Currently only CHAT available")));
                                    }
                                    break;
                            }
                            LOGGER.log(Level.FINE, "Processed successfully");
                        } catch (ParsingException e) {
                            clientSession.write(PROTOCOL.encodeMessage(new Message(Message.Type.SERVICE, "Wrong message format!")));
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Client exception", e);
            } finally {
                LOGGER.log(Level.FINE, "Session removed: {0}", new Object[]{clientHandlers.remove(this)});
            }
            LOGGER.log(Level.INFO, "Client gone");
        }


        private boolean authenticateClient(String login) {
            // TODO: implement me
            return true;
        }
    }
}
