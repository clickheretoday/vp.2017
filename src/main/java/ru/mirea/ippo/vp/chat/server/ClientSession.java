package ru.mirea.ippo.vp.chat.server;

import java.io.*;
import java.net.Socket;
import java.util.UUID;
import java.util.logging.Logger;

public class ClientSession implements Closeable {
    private static final Logger LOGGER = Logger.getLogger(ClientSession.class.getName());

    private final BufferedReader clientIn;
    private final BufferedWriter clientOut;
    private final Socket socket;
    private final InputStream inputStream;
    private final OutputStream outputStream;
    private final String sessionID;
    private State state = State.CONNECTED;
    private String login;

    public ClientSession(Socket socket) throws IOException {
        this.socket = socket;

        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();

        clientIn = new BufferedReader(new InputStreamReader(getInputStream()));
        clientOut = new BufferedWriter(new OutputStreamWriter(getOutputStream()));

        this.sessionID = UUID.randomUUID().toString();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String read() throws IOException {
        String response = null;
        while ((response = clientIn.readLine()) != null) {
            return response;
        }
        return response;
    }

    public void write(String msg) throws IOException {
        clientOut.write(msg);
        clientOut.newLine();
        clientOut.flush();
    }

    public void close() throws IOException {
        LOGGER.info("Closing session " + sessionID);
        socket.close();
    }

    public Socket getSocket() {
        return socket;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public String getSessionID() {
        return sessionID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientSession that = (ClientSession) o;

        return sessionID.equals(that.sessionID);
    }

    @Override
    public int hashCode() {
        return sessionID.hashCode();
    }

    @Override
    public String toString() {
        return "ClientSession{" +
                "sessionID='" + sessionID + '\'' +
                ", login='" + login + '\'' +
                '}';
    }

    public enum State {
        CONNECTED, LOGGED_IN
    }
}
