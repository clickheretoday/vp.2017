package ru.mirea.ippo.vp.chat.client;

import ru.mirea.ippo.vp.chat.server.ServerStarter;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IIS on 09.04.2016.
 */
public class Client {
    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private static final Level level = Level.INFO;

    private static AtomicBoolean stop = new AtomicBoolean(false);

    public static void main(String[] args) {
        for (Handler handler : LOGGER.getParent().getHandlers()) {
            handler.setLevel(level);
        }
        LOGGER.setLevel(level);

        LOGGER.info("Client started");
        try (Socket socket = new Socket("localhost", ServerStarter.PORT)) {
            LOGGER.info("Connected to server");

            final Thread writer = new Thread(() -> {
                LOGGER.log(Level.FINE, "Writer run");

                try (BufferedReader consoleIn = new BufferedReader(new InputStreamReader(System.in));
                     BufferedWriter serverOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
                    String clientInput = null;
                    LOGGER.log(Level.FINEST, "Waiting for input");
                    while (!stop.get() && (clientInput = consoleIn.readLine()) != null) {
                        LOGGER.log(Level.FINE, "You input: {0}", new Object[]{clientInput});
                        if ("quit".equals(clientInput)) {
                            stop.set(true);
                            break;
                        }
                        serverOut.write(clientInput);
                        serverOut.newLine();
                        serverOut.flush();

                        LOGGER.log(Level.FINE, "Sent successfully");
                    }
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "OH NO", e);
                }
            });
            writer.start();

            Thread reader = new Thread(() -> {
                LOGGER.log(Level.FINE, "Reader run");
                try (BufferedReader serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                    String serverInput = null;
                    while (!stop.get() && (serverInput = serverIn.readLine()) != null) {
                        LOGGER.log(Level.INFO, "Server sent: {0}", new Object[]{serverInput});
                    }
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "OH NO", e);
                } finally {
                    stop.set(true);
                }
            });
            reader.start();

            writer.join();
            reader.join();
        } catch (IOException | InterruptedException e) {
            LOGGER.log(Level.SEVERE, "OH NO", e);
        } finally {
            LOGGER.log(Level.FINEST, "Will always be printed");
        }
    }
}
